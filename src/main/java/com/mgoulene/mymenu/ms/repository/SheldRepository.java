package com.mgoulene.mymenu.ms.repository;

import com.mgoulene.mymenu.ms.domain.Sheld;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Sheld entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SheldRepository extends JpaRepository<Sheld, Long>, JpaSpecificationExecutor<Sheld> {}
