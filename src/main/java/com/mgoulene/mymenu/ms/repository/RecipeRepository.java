package com.mgoulene.mymenu.ms.repository;

import com.mgoulene.mymenu.ms.domain.Recipe;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Recipe entity.
 */
@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long>, JpaSpecificationExecutor<Recipe> {
    @Query(
        value = "select distinct recipe from Recipe recipe left join fetch recipe.tags left join fetch recipe.ingredients left join fetch recipe.recipePictures left join fetch recipe.recipeSteps",
        countQuery = "select count(distinct recipe) from Recipe recipe"
    )
    Page<Recipe> findAllWithEagerRelationships(Pageable pageable);

    @Query(
        "select distinct recipe from Recipe recipe left join fetch recipe.tags left join fetch recipe.ingredients left join fetch recipe.recipePictures left join fetch recipe.recipeSteps"
    )
    List<Recipe> findAllWithEagerRelationships();

    @Query(
        "select recipe from Recipe recipe left join fetch recipe.tags left join fetch recipe.ingredients left join fetch recipe.recipePictures left join fetch recipe.recipeSteps where recipe.id =:id"
    )
    Optional<Recipe> findOneWithEagerRelationships(@Param("id") Long id);
}
