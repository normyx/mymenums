package com.mgoulene.mymenu.ms.repository;

import com.mgoulene.mymenu.ms.domain.ReceipePicture;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ReceipePicture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReceipePictureRepository extends JpaRepository<ReceipePicture, Long>, JpaSpecificationExecutor<ReceipePicture> {}
