package com.mgoulene.mymenu.ms.repository;

import com.mgoulene.mymenu.ms.domain.RecipeStep;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RecipeStep entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RecipeStepRepository extends JpaRepository<RecipeStep, Long>, JpaSpecificationExecutor<RecipeStep> {}
