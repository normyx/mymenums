package com.mgoulene.mymenu.ms.service.mapper;

import com.mgoulene.mymenu.ms.domain.*;
import com.mgoulene.mymenu.ms.service.dto.RecipeDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Recipe} and its DTO {@link RecipeDTO}.
 */
@Mapper(componentModel = "spring", uses = { TagMapper.class, IngredientMapper.class, ReceipePictureMapper.class, RecipeStepMapper.class })
public interface RecipeMapper extends EntityMapper<RecipeDTO, Recipe> {
    @Mapping(target = "tags", source = "tags", qualifiedByName = "labelSet")
    @Mapping(target = "ingredients", source = "ingredients", qualifiedByName = "quantityTypeSet")
    @Mapping(target = "recipePictures", source = "recipePictures", qualifiedByName = "labelSet")
    @Mapping(target = "recipeSteps", source = "recipeSteps", qualifiedByName = "stepDescriptionSet")
    RecipeDTO toDto(Recipe s);

    @Mapping(target = "removeTag", ignore = true)
    @Mapping(target = "removeIngredient", ignore = true)
    @Mapping(target = "removeRecipePicture", ignore = true)
    @Mapping(target = "removeRecipeStep", ignore = true)
    Recipe toEntity(RecipeDTO recipeDTO);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    RecipeDTO toDtoName(Recipe recipe);
}
