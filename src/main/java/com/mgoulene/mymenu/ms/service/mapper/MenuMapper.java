package com.mgoulene.mymenu.ms.service.mapper;

import com.mgoulene.mymenu.ms.domain.*;
import com.mgoulene.mymenu.ms.service.dto.MenuDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Menu} and its DTO {@link MenuDTO}.
 */
@Mapper(componentModel = "spring", uses = { RecipeMapper.class })
public interface MenuMapper extends EntityMapper<MenuDTO, Menu> {
    @Mapping(target = "starter", source = "starter", qualifiedByName = "name")
    @Mapping(target = "main", source = "main", qualifiedByName = "name")
    @Mapping(target = "dessert", source = "dessert", qualifiedByName = "name")
    MenuDTO toDto(Menu s);
}
