package com.mgoulene.mymenu.ms.service.mapper;

import com.mgoulene.mymenu.ms.domain.*;
import com.mgoulene.mymenu.ms.service.dto.TagDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tag} and its DTO {@link TagDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TagMapper extends EntityMapper<TagDTO, Tag> {
    @Named("labelSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "label", source = "label")
    Set<TagDTO> toDtoLabelSet(Set<Tag> tag);
}
