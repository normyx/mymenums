package com.mgoulene.mymenu.ms.service.mapper;

import com.mgoulene.mymenu.ms.domain.*;
import com.mgoulene.mymenu.ms.service.dto.ReceipePictureDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ReceipePicture} and its DTO {@link ReceipePictureDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReceipePictureMapper extends EntityMapper<ReceipePictureDTO, ReceipePicture> {
    @Named("labelSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "label", source = "label")
    Set<ReceipePictureDTO> toDtoLabelSet(Set<ReceipePicture> receipePicture);
}
