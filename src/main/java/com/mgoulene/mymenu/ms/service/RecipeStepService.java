package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.RecipeStep;
import com.mgoulene.mymenu.ms.repository.RecipeStepRepository;
import com.mgoulene.mymenu.ms.service.dto.RecipeStepDTO;
import com.mgoulene.mymenu.ms.service.mapper.RecipeStepMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RecipeStep}.
 */
@Service
@Transactional
public class RecipeStepService {

    private final Logger log = LoggerFactory.getLogger(RecipeStepService.class);

    private final RecipeStepRepository recipeStepRepository;

    private final RecipeStepMapper recipeStepMapper;

    public RecipeStepService(RecipeStepRepository recipeStepRepository, RecipeStepMapper recipeStepMapper) {
        this.recipeStepRepository = recipeStepRepository;
        this.recipeStepMapper = recipeStepMapper;
    }

    /**
     * Save a recipeStep.
     *
     * @param recipeStepDTO the entity to save.
     * @return the persisted entity.
     */
    public RecipeStepDTO save(RecipeStepDTO recipeStepDTO) {
        log.debug("Request to save RecipeStep : {}", recipeStepDTO);
        RecipeStep recipeStep = recipeStepMapper.toEntity(recipeStepDTO);
        recipeStep = recipeStepRepository.save(recipeStep);
        return recipeStepMapper.toDto(recipeStep);
    }

    /**
     * Partially update a recipeStep.
     *
     * @param recipeStepDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<RecipeStepDTO> partialUpdate(RecipeStepDTO recipeStepDTO) {
        log.debug("Request to partially update RecipeStep : {}", recipeStepDTO);

        return recipeStepRepository
            .findById(recipeStepDTO.getId())
            .map(
                existingRecipeStep -> {
                    recipeStepMapper.partialUpdate(existingRecipeStep, recipeStepDTO);
                    return existingRecipeStep;
                }
            )
            .map(recipeStepRepository::save)
            .map(recipeStepMapper::toDto);
    }

    /**
     * Get all the recipeSteps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipeStepDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RecipeSteps");
        return recipeStepRepository.findAll(pageable).map(recipeStepMapper::toDto);
    }

    /**
     * Get one recipeStep by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RecipeStepDTO> findOne(Long id) {
        log.debug("Request to get RecipeStep : {}", id);
        return recipeStepRepository.findById(id).map(recipeStepMapper::toDto);
    }

    /**
     * Delete the recipeStep by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RecipeStep : {}", id);
        recipeStepRepository.deleteById(id);
    }
}
