package com.mgoulene.mymenu.ms.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.ms.domain.RecipeStep} entity.
 */
public class RecipeStepDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 1)
    private Integer stepNum;

    @Size(max = 4000)
    private String stepDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStepNum() {
        return stepNum;
    }

    public void setStepNum(Integer stepNum) {
        this.stepNum = stepNum;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecipeStepDTO)) {
            return false;
        }

        RecipeStepDTO recipeStepDTO = (RecipeStepDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, recipeStepDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipeStepDTO{" +
            "id=" + getId() +
            ", stepNum=" + getStepNum() +
            ", stepDescription='" + getStepDescription() + "'" +
            "}";
    }
}
