package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.Sheld;
import com.mgoulene.mymenu.ms.repository.SheldRepository;
import com.mgoulene.mymenu.ms.service.dto.SheldDTO;
import com.mgoulene.mymenu.ms.service.mapper.SheldMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sheld}.
 */
@Service
@Transactional
public class SheldService {

    private final Logger log = LoggerFactory.getLogger(SheldService.class);

    private final SheldRepository sheldRepository;

    private final SheldMapper sheldMapper;

    public SheldService(SheldRepository sheldRepository, SheldMapper sheldMapper) {
        this.sheldRepository = sheldRepository;
        this.sheldMapper = sheldMapper;
    }

    /**
     * Save a sheld.
     *
     * @param sheldDTO the entity to save.
     * @return the persisted entity.
     */
    public SheldDTO save(SheldDTO sheldDTO) {
        log.debug("Request to save Sheld : {}", sheldDTO);
        Sheld sheld = sheldMapper.toEntity(sheldDTO);
        sheld = sheldRepository.save(sheld);
        return sheldMapper.toDto(sheld);
    }

    /**
     * Partially update a sheld.
     *
     * @param sheldDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SheldDTO> partialUpdate(SheldDTO sheldDTO) {
        log.debug("Request to partially update Sheld : {}", sheldDTO);

        return sheldRepository
            .findById(sheldDTO.getId())
            .map(
                existingSheld -> {
                    sheldMapper.partialUpdate(existingSheld, sheldDTO);
                    return existingSheld;
                }
            )
            .map(sheldRepository::save)
            .map(sheldMapper::toDto);
    }

    /**
     * Get all the shelds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SheldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shelds");
        return sheldRepository.findAll(pageable).map(sheldMapper::toDto);
    }

    /**
     * Get one sheld by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SheldDTO> findOne(Long id) {
        log.debug("Request to get Sheld : {}", id);
        return sheldRepository.findById(id).map(sheldMapper::toDto);
    }

    /**
     * Delete the sheld by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sheld : {}", id);
        sheldRepository.deleteById(id);
    }
}
