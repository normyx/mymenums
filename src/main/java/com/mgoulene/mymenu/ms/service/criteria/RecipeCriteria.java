package com.mgoulene.mymenu.ms.service.criteria;

import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.RecipeType;
import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.mymenu.ms.domain.Recipe} entity. This class is used
 * in {@link com.mgoulene.mymenu.ms.web.rest.RecipeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /recipes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RecipeCriteria implements Serializable, Criteria {

    /**
     * Class for filtering PreferredSeason
     */
    public static class PreferredSeasonFilter extends Filter<PreferredSeason> {

        public PreferredSeasonFilter() {}

        public PreferredSeasonFilter(PreferredSeasonFilter filter) {
            super(filter);
        }

        @Override
        public PreferredSeasonFilter copy() {
            return new PreferredSeasonFilter(this);
        }
    }

    /**
     * Class for filtering RecipeType
     */
    public static class RecipeTypeFilter extends Filter<RecipeType> {

        public RecipeTypeFilter() {}

        public RecipeTypeFilter(RecipeTypeFilter filter) {
            super(filter);
        }

        @Override
        public RecipeTypeFilter copy() {
            return new RecipeTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter description;

    private IntegerFilter preparationDuration;

    private IntegerFilter restDuraction;

    private IntegerFilter cookingDuration;

    private PreferredSeasonFilter winterPreferrency;

    private PreferredSeasonFilter springPreferrency;

    private PreferredSeasonFilter summerPreferrency;

    private PreferredSeasonFilter autumnPreferrency;

    private IntegerFilter numberOfPeople;

    private RecipeTypeFilter recipeType;

    private StringFilter recipeUrl;

    private StringFilter shortSteps;

    private BooleanFilter isShortDescRecipe;

    private LongFilter tagId;

    private LongFilter ingredientId;

    private LongFilter recipePictureId;

    private LongFilter recipeStepId;

    public RecipeCriteria() {}

    public RecipeCriteria(RecipeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.preparationDuration = other.preparationDuration == null ? null : other.preparationDuration.copy();
        this.restDuraction = other.restDuraction == null ? null : other.restDuraction.copy();
        this.cookingDuration = other.cookingDuration == null ? null : other.cookingDuration.copy();
        this.winterPreferrency = other.winterPreferrency == null ? null : other.winterPreferrency.copy();
        this.springPreferrency = other.springPreferrency == null ? null : other.springPreferrency.copy();
        this.summerPreferrency = other.summerPreferrency == null ? null : other.summerPreferrency.copy();
        this.autumnPreferrency = other.autumnPreferrency == null ? null : other.autumnPreferrency.copy();
        this.numberOfPeople = other.numberOfPeople == null ? null : other.numberOfPeople.copy();
        this.recipeType = other.recipeType == null ? null : other.recipeType.copy();
        this.recipeUrl = other.recipeUrl == null ? null : other.recipeUrl.copy();
        this.shortSteps = other.shortSteps == null ? null : other.shortSteps.copy();
        this.isShortDescRecipe = other.isShortDescRecipe == null ? null : other.isShortDescRecipe.copy();
        this.tagId = other.tagId == null ? null : other.tagId.copy();
        this.ingredientId = other.ingredientId == null ? null : other.ingredientId.copy();
        this.recipePictureId = other.recipePictureId == null ? null : other.recipePictureId.copy();
        this.recipeStepId = other.recipeStepId == null ? null : other.recipeStepId.copy();
    }

    @Override
    public RecipeCriteria copy() {
        return new RecipeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getPreparationDuration() {
        return preparationDuration;
    }

    public IntegerFilter preparationDuration() {
        if (preparationDuration == null) {
            preparationDuration = new IntegerFilter();
        }
        return preparationDuration;
    }

    public void setPreparationDuration(IntegerFilter preparationDuration) {
        this.preparationDuration = preparationDuration;
    }

    public IntegerFilter getRestDuraction() {
        return restDuraction;
    }

    public IntegerFilter restDuraction() {
        if (restDuraction == null) {
            restDuraction = new IntegerFilter();
        }
        return restDuraction;
    }

    public void setRestDuraction(IntegerFilter restDuraction) {
        this.restDuraction = restDuraction;
    }

    public IntegerFilter getCookingDuration() {
        return cookingDuration;
    }

    public IntegerFilter cookingDuration() {
        if (cookingDuration == null) {
            cookingDuration = new IntegerFilter();
        }
        return cookingDuration;
    }

    public void setCookingDuration(IntegerFilter cookingDuration) {
        this.cookingDuration = cookingDuration;
    }

    public PreferredSeasonFilter getWinterPreferrency() {
        return winterPreferrency;
    }

    public PreferredSeasonFilter winterPreferrency() {
        if (winterPreferrency == null) {
            winterPreferrency = new PreferredSeasonFilter();
        }
        return winterPreferrency;
    }

    public void setWinterPreferrency(PreferredSeasonFilter winterPreferrency) {
        this.winterPreferrency = winterPreferrency;
    }

    public PreferredSeasonFilter getSpringPreferrency() {
        return springPreferrency;
    }

    public PreferredSeasonFilter springPreferrency() {
        if (springPreferrency == null) {
            springPreferrency = new PreferredSeasonFilter();
        }
        return springPreferrency;
    }

    public void setSpringPreferrency(PreferredSeasonFilter springPreferrency) {
        this.springPreferrency = springPreferrency;
    }

    public PreferredSeasonFilter getSummerPreferrency() {
        return summerPreferrency;
    }

    public PreferredSeasonFilter summerPreferrency() {
        if (summerPreferrency == null) {
            summerPreferrency = new PreferredSeasonFilter();
        }
        return summerPreferrency;
    }

    public void setSummerPreferrency(PreferredSeasonFilter summerPreferrency) {
        this.summerPreferrency = summerPreferrency;
    }

    public PreferredSeasonFilter getAutumnPreferrency() {
        return autumnPreferrency;
    }

    public PreferredSeasonFilter autumnPreferrency() {
        if (autumnPreferrency == null) {
            autumnPreferrency = new PreferredSeasonFilter();
        }
        return autumnPreferrency;
    }

    public void setAutumnPreferrency(PreferredSeasonFilter autumnPreferrency) {
        this.autumnPreferrency = autumnPreferrency;
    }

    public IntegerFilter getNumberOfPeople() {
        return numberOfPeople;
    }

    public IntegerFilter numberOfPeople() {
        if (numberOfPeople == null) {
            numberOfPeople = new IntegerFilter();
        }
        return numberOfPeople;
    }

    public void setNumberOfPeople(IntegerFilter numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public RecipeTypeFilter getRecipeType() {
        return recipeType;
    }

    public RecipeTypeFilter recipeType() {
        if (recipeType == null) {
            recipeType = new RecipeTypeFilter();
        }
        return recipeType;
    }

    public void setRecipeType(RecipeTypeFilter recipeType) {
        this.recipeType = recipeType;
    }

    public StringFilter getRecipeUrl() {
        return recipeUrl;
    }

    public StringFilter recipeUrl() {
        if (recipeUrl == null) {
            recipeUrl = new StringFilter();
        }
        return recipeUrl;
    }

    public void setRecipeUrl(StringFilter recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public StringFilter getShortSteps() {
        return shortSteps;
    }

    public StringFilter shortSteps() {
        if (shortSteps == null) {
            shortSteps = new StringFilter();
        }
        return shortSteps;
    }

    public void setShortSteps(StringFilter shortSteps) {
        this.shortSteps = shortSteps;
    }

    public BooleanFilter getIsShortDescRecipe() {
        return isShortDescRecipe;
    }

    public BooleanFilter isShortDescRecipe() {
        if (isShortDescRecipe == null) {
            isShortDescRecipe = new BooleanFilter();
        }
        return isShortDescRecipe;
    }

    public void setIsShortDescRecipe(BooleanFilter isShortDescRecipe) {
        this.isShortDescRecipe = isShortDescRecipe;
    }

    public LongFilter getTagId() {
        return tagId;
    }

    public LongFilter tagId() {
        if (tagId == null) {
            tagId = new LongFilter();
        }
        return tagId;
    }

    public void setTagId(LongFilter tagId) {
        this.tagId = tagId;
    }

    public LongFilter getIngredientId() {
        return ingredientId;
    }

    public LongFilter ingredientId() {
        if (ingredientId == null) {
            ingredientId = new LongFilter();
        }
        return ingredientId;
    }

    public void setIngredientId(LongFilter ingredientId) {
        this.ingredientId = ingredientId;
    }

    public LongFilter getRecipePictureId() {
        return recipePictureId;
    }

    public LongFilter recipePictureId() {
        if (recipePictureId == null) {
            recipePictureId = new LongFilter();
        }
        return recipePictureId;
    }

    public void setRecipePictureId(LongFilter recipePictureId) {
        this.recipePictureId = recipePictureId;
    }

    public LongFilter getRecipeStepId() {
        return recipeStepId;
    }

    public LongFilter recipeStepId() {
        if (recipeStepId == null) {
            recipeStepId = new LongFilter();
        }
        return recipeStepId;
    }

    public void setRecipeStepId(LongFilter recipeStepId) {
        this.recipeStepId = recipeStepId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RecipeCriteria that = (RecipeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(preparationDuration, that.preparationDuration) &&
            Objects.equals(restDuraction, that.restDuraction) &&
            Objects.equals(cookingDuration, that.cookingDuration) &&
            Objects.equals(winterPreferrency, that.winterPreferrency) &&
            Objects.equals(springPreferrency, that.springPreferrency) &&
            Objects.equals(summerPreferrency, that.summerPreferrency) &&
            Objects.equals(autumnPreferrency, that.autumnPreferrency) &&
            Objects.equals(numberOfPeople, that.numberOfPeople) &&
            Objects.equals(recipeType, that.recipeType) &&
            Objects.equals(recipeUrl, that.recipeUrl) &&
            Objects.equals(shortSteps, that.shortSteps) &&
            Objects.equals(isShortDescRecipe, that.isShortDescRecipe) &&
            Objects.equals(tagId, that.tagId) &&
            Objects.equals(ingredientId, that.ingredientId) &&
            Objects.equals(recipePictureId, that.recipePictureId) &&
            Objects.equals(recipeStepId, that.recipeStepId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            description,
            preparationDuration,
            restDuraction,
            cookingDuration,
            winterPreferrency,
            springPreferrency,
            summerPreferrency,
            autumnPreferrency,
            numberOfPeople,
            recipeType,
            recipeUrl,
            shortSteps,
            isShortDescRecipe,
            tagId,
            ingredientId,
            recipePictureId,
            recipeStepId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (preparationDuration != null ? "preparationDuration=" + preparationDuration + ", " : "") +
            (restDuraction != null ? "restDuraction=" + restDuraction + ", " : "") +
            (cookingDuration != null ? "cookingDuration=" + cookingDuration + ", " : "") +
            (winterPreferrency != null ? "winterPreferrency=" + winterPreferrency + ", " : "") +
            (springPreferrency != null ? "springPreferrency=" + springPreferrency + ", " : "") +
            (summerPreferrency != null ? "summerPreferrency=" + summerPreferrency + ", " : "") +
            (autumnPreferrency != null ? "autumnPreferrency=" + autumnPreferrency + ", " : "") +
            (numberOfPeople != null ? "numberOfPeople=" + numberOfPeople + ", " : "") +
            (recipeType != null ? "recipeType=" + recipeType + ", " : "") +
            (recipeUrl != null ? "recipeUrl=" + recipeUrl + ", " : "") +
            (shortSteps != null ? "shortSteps=" + shortSteps + ", " : "") +
            (isShortDescRecipe != null ? "isShortDescRecipe=" + isShortDescRecipe + ", " : "") +
            (tagId != null ? "tagId=" + tagId + ", " : "") +
            (ingredientId != null ? "ingredientId=" + ingredientId + ", " : "") +
            (recipePictureId != null ? "recipePictureId=" + recipePictureId + ", " : "") +
            (recipeStepId != null ? "recipeStepId=" + recipeStepId + ", " : "") +
            "}";
    }
}
