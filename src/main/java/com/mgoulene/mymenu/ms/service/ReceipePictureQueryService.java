package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.*; // for static metamodels
import com.mgoulene.mymenu.ms.domain.ReceipePicture;
import com.mgoulene.mymenu.ms.repository.ReceipePictureRepository;
import com.mgoulene.mymenu.ms.service.criteria.ReceipePictureCriteria;
import com.mgoulene.mymenu.ms.service.dto.ReceipePictureDTO;
import com.mgoulene.mymenu.ms.service.mapper.ReceipePictureMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ReceipePicture} entities in the database.
 * The main input is a {@link ReceipePictureCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ReceipePictureDTO} or a {@link Page} of {@link ReceipePictureDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ReceipePictureQueryService extends QueryService<ReceipePicture> {

    private final Logger log = LoggerFactory.getLogger(ReceipePictureQueryService.class);

    private final ReceipePictureRepository receipePictureRepository;

    private final ReceipePictureMapper receipePictureMapper;

    public ReceipePictureQueryService(ReceipePictureRepository receipePictureRepository, ReceipePictureMapper receipePictureMapper) {
        this.receipePictureRepository = receipePictureRepository;
        this.receipePictureMapper = receipePictureMapper;
    }

    /**
     * Return a {@link List} of {@link ReceipePictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ReceipePictureDTO> findByCriteria(ReceipePictureCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ReceipePicture> specification = createSpecification(criteria);
        return receipePictureMapper.toDto(receipePictureRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ReceipePictureDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ReceipePictureDTO> findByCriteria(ReceipePictureCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ReceipePicture> specification = createSpecification(criteria);
        return receipePictureRepository.findAll(specification, page).map(receipePictureMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ReceipePictureCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ReceipePicture> specification = createSpecification(criteria);
        return receipePictureRepository.count(specification);
    }

    /**
     * Function to convert {@link ReceipePictureCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ReceipePicture> createSpecification(ReceipePictureCriteria criteria) {
        Specification<ReceipePicture> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ReceipePicture_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ReceipePicture_.label));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), ReceipePicture_.order));
            }
            if (criteria.getRecipeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRecipeId(),
                            root -> root.join(ReceipePicture_.recipes, JoinType.LEFT).get(Recipe_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
