package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.*; // for static metamodels
import com.mgoulene.mymenu.ms.domain.Recipe;
import com.mgoulene.mymenu.ms.repository.RecipeRepository;
import com.mgoulene.mymenu.ms.service.criteria.RecipeCriteria;
import com.mgoulene.mymenu.ms.service.dto.RecipeDTO;
import com.mgoulene.mymenu.ms.service.mapper.RecipeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Recipe} entities in the database.
 * The main input is a {@link RecipeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RecipeDTO} or a {@link Page} of {@link RecipeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RecipeQueryService extends QueryService<Recipe> {

    private final Logger log = LoggerFactory.getLogger(RecipeQueryService.class);

    private final RecipeRepository recipeRepository;

    private final RecipeMapper recipeMapper;

    public RecipeQueryService(RecipeRepository recipeRepository, RecipeMapper recipeMapper) {
        this.recipeRepository = recipeRepository;
        this.recipeMapper = recipeMapper;
    }

    /**
     * Return a {@link List} of {@link RecipeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RecipeDTO> findByCriteria(RecipeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Recipe> specification = createSpecification(criteria);
        return recipeMapper.toDto(recipeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RecipeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RecipeDTO> findByCriteria(RecipeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Recipe> specification = createSpecification(criteria);
        return recipeRepository.findAll(specification, page).map(recipeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RecipeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Recipe> specification = createSpecification(criteria);
        return recipeRepository.count(specification);
    }

    /**
     * Function to convert {@link RecipeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Recipe> createSpecification(RecipeCriteria criteria) {
        Specification<Recipe> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Recipe_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Recipe_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Recipe_.description));
            }
            if (criteria.getPreparationDuration() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPreparationDuration(), Recipe_.preparationDuration));
            }
            if (criteria.getRestDuraction() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRestDuraction(), Recipe_.restDuraction));
            }
            if (criteria.getCookingDuration() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCookingDuration(), Recipe_.cookingDuration));
            }
            if (criteria.getWinterPreferrency() != null) {
                specification = specification.and(buildSpecification(criteria.getWinterPreferrency(), Recipe_.winterPreferrency));
            }
            if (criteria.getSpringPreferrency() != null) {
                specification = specification.and(buildSpecification(criteria.getSpringPreferrency(), Recipe_.springPreferrency));
            }
            if (criteria.getSummerPreferrency() != null) {
                specification = specification.and(buildSpecification(criteria.getSummerPreferrency(), Recipe_.summerPreferrency));
            }
            if (criteria.getAutumnPreferrency() != null) {
                specification = specification.and(buildSpecification(criteria.getAutumnPreferrency(), Recipe_.autumnPreferrency));
            }
            if (criteria.getNumberOfPeople() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfPeople(), Recipe_.numberOfPeople));
            }
            if (criteria.getRecipeType() != null) {
                specification = specification.and(buildSpecification(criteria.getRecipeType(), Recipe_.recipeType));
            }
            if (criteria.getRecipeUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecipeUrl(), Recipe_.recipeUrl));
            }
            if (criteria.getShortSteps() != null) {
                specification = specification.and(buildStringSpecification(criteria.getShortSteps(), Recipe_.shortSteps));
            }
            if (criteria.getIsShortDescRecipe() != null) {
                specification = specification.and(buildSpecification(criteria.getIsShortDescRecipe(), Recipe_.isShortDescRecipe));
            }
            if (criteria.getTagId() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getTagId(), root -> root.join(Recipe_.tags, JoinType.LEFT).get(Tag_.id)));
            }
            if (criteria.getIngredientId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getIngredientId(),
                            root -> root.join(Recipe_.ingredients, JoinType.LEFT).get(Ingredient_.id)
                        )
                    );
            }
            if (criteria.getRecipePictureId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRecipePictureId(),
                            root -> root.join(Recipe_.recipePictures, JoinType.LEFT).get(ReceipePicture_.id)
                        )
                    );
            }
            if (criteria.getRecipeStepId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRecipeStepId(),
                            root -> root.join(Recipe_.recipeSteps, JoinType.LEFT).get(RecipeStep_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
