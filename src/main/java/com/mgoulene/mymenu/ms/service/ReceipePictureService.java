package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.ReceipePicture;
import com.mgoulene.mymenu.ms.repository.ReceipePictureRepository;
import com.mgoulene.mymenu.ms.service.dto.ReceipePictureDTO;
import com.mgoulene.mymenu.ms.service.mapper.ReceipePictureMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ReceipePicture}.
 */
@Service
@Transactional
public class ReceipePictureService {

    private final Logger log = LoggerFactory.getLogger(ReceipePictureService.class);

    private final ReceipePictureRepository receipePictureRepository;

    private final ReceipePictureMapper receipePictureMapper;

    public ReceipePictureService(ReceipePictureRepository receipePictureRepository, ReceipePictureMapper receipePictureMapper) {
        this.receipePictureRepository = receipePictureRepository;
        this.receipePictureMapper = receipePictureMapper;
    }

    /**
     * Save a receipePicture.
     *
     * @param receipePictureDTO the entity to save.
     * @return the persisted entity.
     */
    public ReceipePictureDTO save(ReceipePictureDTO receipePictureDTO) {
        log.debug("Request to save ReceipePicture : {}", receipePictureDTO);
        ReceipePicture receipePicture = receipePictureMapper.toEntity(receipePictureDTO);
        receipePicture = receipePictureRepository.save(receipePicture);
        return receipePictureMapper.toDto(receipePicture);
    }

    /**
     * Partially update a receipePicture.
     *
     * @param receipePictureDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ReceipePictureDTO> partialUpdate(ReceipePictureDTO receipePictureDTO) {
        log.debug("Request to partially update ReceipePicture : {}", receipePictureDTO);

        return receipePictureRepository
            .findById(receipePictureDTO.getId())
            .map(
                existingReceipePicture -> {
                    receipePictureMapper.partialUpdate(existingReceipePicture, receipePictureDTO);
                    return existingReceipePicture;
                }
            )
            .map(receipePictureRepository::save)
            .map(receipePictureMapper::toDto);
    }

    /**
     * Get all the receipePictures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ReceipePictureDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReceipePictures");
        return receipePictureRepository.findAll(pageable).map(receipePictureMapper::toDto);
    }

    /**
     * Get one receipePicture by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ReceipePictureDTO> findOne(Long id) {
        log.debug("Request to get ReceipePicture : {}", id);
        return receipePictureRepository.findById(id).map(receipePictureMapper::toDto);
    }

    /**
     * Delete the receipePicture by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ReceipePicture : {}", id);
        receipePictureRepository.deleteById(id);
    }
}
