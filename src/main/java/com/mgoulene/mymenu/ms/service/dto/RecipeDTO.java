package com.mgoulene.mymenu.ms.service.dto;

import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.RecipeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.ms.domain.Recipe} entity.
 */
@ApiModel(description = "The Recipe entity.\n@author A true hipster")
public class RecipeDTO implements Serializable {

    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 400)
    @ApiModelProperty(value = "fieldName", required = true)
    private String name;

    @Size(max = 10000)
    private String description;

    @Min(value = 0)
    private Integer preparationDuration;

    @Min(value = 0)
    private Integer restDuraction;

    @Min(value = 0)
    private Integer cookingDuration;

    private PreferredSeason winterPreferrency;

    private PreferredSeason springPreferrency;

    private PreferredSeason summerPreferrency;

    private PreferredSeason autumnPreferrency;

    @Min(value = 1)
    private Integer numberOfPeople;

    @NotNull
    private RecipeType recipeType;

    @Size(min = 5, max = 200)
    private String recipeUrl;

    @Size(max = 40000)
    private String shortSteps;

    @NotNull
    private Boolean isShortDescRecipe;

    private Set<TagDTO> tags = new HashSet<>();

    private Set<IngredientDTO> ingredients = new HashSet<>();

    private Set<ReceipePictureDTO> recipePictures = new HashSet<>();

    private Set<RecipeStepDTO> recipeSteps = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPreparationDuration() {
        return preparationDuration;
    }

    public void setPreparationDuration(Integer preparationDuration) {
        this.preparationDuration = preparationDuration;
    }

    public Integer getRestDuraction() {
        return restDuraction;
    }

    public void setRestDuraction(Integer restDuraction) {
        this.restDuraction = restDuraction;
    }

    public Integer getCookingDuration() {
        return cookingDuration;
    }

    public void setCookingDuration(Integer cookingDuration) {
        this.cookingDuration = cookingDuration;
    }

    public PreferredSeason getWinterPreferrency() {
        return winterPreferrency;
    }

    public void setWinterPreferrency(PreferredSeason winterPreferrency) {
        this.winterPreferrency = winterPreferrency;
    }

    public PreferredSeason getSpringPreferrency() {
        return springPreferrency;
    }

    public void setSpringPreferrency(PreferredSeason springPreferrency) {
        this.springPreferrency = springPreferrency;
    }

    public PreferredSeason getSummerPreferrency() {
        return summerPreferrency;
    }

    public void setSummerPreferrency(PreferredSeason summerPreferrency) {
        this.summerPreferrency = summerPreferrency;
    }

    public PreferredSeason getAutumnPreferrency() {
        return autumnPreferrency;
    }

    public void setAutumnPreferrency(PreferredSeason autumnPreferrency) {
        this.autumnPreferrency = autumnPreferrency;
    }

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public RecipeType getRecipeType() {
        return recipeType;
    }

    public void setRecipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
    }

    public String getRecipeUrl() {
        return recipeUrl;
    }

    public void setRecipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public String getShortSteps() {
        return shortSteps;
    }

    public void setShortSteps(String shortSteps) {
        this.shortSteps = shortSteps;
    }

    public Boolean getIsShortDescRecipe() {
        return isShortDescRecipe;
    }

    public void setIsShortDescRecipe(Boolean isShortDescRecipe) {
        this.isShortDescRecipe = isShortDescRecipe;
    }

    public Set<TagDTO> getTags() {
        return tags;
    }

    public void setTags(Set<TagDTO> tags) {
        this.tags = tags;
    }

    public Set<IngredientDTO> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<IngredientDTO> ingredients) {
        this.ingredients = ingredients;
    }

    public Set<ReceipePictureDTO> getRecipePictures() {
        return recipePictures;
    }

    public void setRecipePictures(Set<ReceipePictureDTO> recipePictures) {
        this.recipePictures = recipePictures;
    }

    public Set<RecipeStepDTO> getRecipeSteps() {
        return recipeSteps;
    }

    public void setRecipeSteps(Set<RecipeStepDTO> recipeSteps) {
        this.recipeSteps = recipeSteps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecipeDTO)) {
            return false;
        }

        RecipeDTO recipeDTO = (RecipeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, recipeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", preparationDuration=" + getPreparationDuration() +
            ", restDuraction=" + getRestDuraction() +
            ", cookingDuration=" + getCookingDuration() +
            ", winterPreferrency='" + getWinterPreferrency() + "'" +
            ", springPreferrency='" + getSpringPreferrency() + "'" +
            ", summerPreferrency='" + getSummerPreferrency() + "'" +
            ", autumnPreferrency='" + getAutumnPreferrency() + "'" +
            ", numberOfPeople=" + getNumberOfPeople() +
            ", recipeType='" + getRecipeType() + "'" +
            ", recipeUrl='" + getRecipeUrl() + "'" +
            ", shortSteps='" + getShortSteps() + "'" +
            ", isShortDescRecipe='" + getIsShortDescRecipe() + "'" +
            ", tags=" + getTags() +
            ", ingredients=" + getIngredients() +
            ", recipePictures=" + getRecipePictures() +
            ", recipeSteps=" + getRecipeSteps() +
            "}";
    }
}
