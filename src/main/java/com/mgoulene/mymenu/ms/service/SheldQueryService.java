package com.mgoulene.mymenu.ms.service;

import com.mgoulene.mymenu.ms.domain.*; // for static metamodels
import com.mgoulene.mymenu.ms.domain.Sheld;
import com.mgoulene.mymenu.ms.repository.SheldRepository;
import com.mgoulene.mymenu.ms.service.criteria.SheldCriteria;
import com.mgoulene.mymenu.ms.service.dto.SheldDTO;
import com.mgoulene.mymenu.ms.service.mapper.SheldMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Sheld} entities in the database.
 * The main input is a {@link SheldCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SheldDTO} or a {@link Page} of {@link SheldDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SheldQueryService extends QueryService<Sheld> {

    private final Logger log = LoggerFactory.getLogger(SheldQueryService.class);

    private final SheldRepository sheldRepository;

    private final SheldMapper sheldMapper;

    public SheldQueryService(SheldRepository sheldRepository, SheldMapper sheldMapper) {
        this.sheldRepository = sheldRepository;
        this.sheldMapper = sheldMapper;
    }

    /**
     * Return a {@link List} of {@link SheldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SheldDTO> findByCriteria(SheldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sheld> specification = createSpecification(criteria);
        return sheldMapper.toDto(sheldRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SheldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SheldDTO> findByCriteria(SheldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sheld> specification = createSpecification(criteria);
        return sheldRepository.findAll(specification, page).map(sheldMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SheldCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sheld> specification = createSpecification(criteria);
        return sheldRepository.count(specification);
    }

    /**
     * Function to convert {@link SheldCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Sheld> createSpecification(SheldCriteria criteria) {
        Specification<Sheld> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Sheld_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Sheld_.name));
            }
        }
        return specification;
    }
}
