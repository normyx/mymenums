package com.mgoulene.mymenu.ms.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.ms.domain.Product} entity.
 */
@ApiModel(description = "The Product entity.\n@author A true hipster")
public class ProductDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "name", required = true)
    private String name;

    @Lob
    private byte[] picture;

    private String pictureContentType;
    private SheldDTO sheld;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public SheldDTO getSheld() {
        return sheld;
    }

    public void setSheld(SheldDTO sheld) {
        this.sheld = sheld;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDTO)) {
            return false;
        }

        ProductDTO productDTO = (ProductDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, productDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", picture='" + getPicture() + "'" +
            ", sheld=" + getSheld() +
            "}";
    }
}
