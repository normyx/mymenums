package com.mgoulene.mymenu.ms.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mymenu.ms.domain.Sheld} entity.
 */
@ApiModel(description = "The Sheld entity.\n@author A true hipster")
public class SheldDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @NotNull
    @Size(max = 200)
    @ApiModelProperty(value = "name", required = true)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SheldDTO)) {
            return false;
        }

        SheldDTO sheldDTO = (SheldDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, sheldDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SheldDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
