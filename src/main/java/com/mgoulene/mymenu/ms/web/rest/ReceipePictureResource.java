package com.mgoulene.mymenu.ms.web.rest;

import com.mgoulene.mymenu.ms.repository.ReceipePictureRepository;
import com.mgoulene.mymenu.ms.service.ReceipePictureQueryService;
import com.mgoulene.mymenu.ms.service.ReceipePictureService;
import com.mgoulene.mymenu.ms.service.criteria.ReceipePictureCriteria;
import com.mgoulene.mymenu.ms.service.dto.ReceipePictureDTO;
import com.mgoulene.mymenu.ms.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mymenu.ms.domain.ReceipePicture}.
 */
@RestController
@RequestMapping("/api")
public class ReceipePictureResource {

    private final Logger log = LoggerFactory.getLogger(ReceipePictureResource.class);

    private static final String ENTITY_NAME = "mymenumsReceipePicture";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReceipePictureService receipePictureService;

    private final ReceipePictureRepository receipePictureRepository;

    private final ReceipePictureQueryService receipePictureQueryService;

    public ReceipePictureResource(
        ReceipePictureService receipePictureService,
        ReceipePictureRepository receipePictureRepository,
        ReceipePictureQueryService receipePictureQueryService
    ) {
        this.receipePictureService = receipePictureService;
        this.receipePictureRepository = receipePictureRepository;
        this.receipePictureQueryService = receipePictureQueryService;
    }

    /**
     * {@code POST  /receipe-pictures} : Create a new receipePicture.
     *
     * @param receipePictureDTO the receipePictureDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new receipePictureDTO, or with status {@code 400 (Bad Request)} if the receipePicture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/receipe-pictures")
    public ResponseEntity<ReceipePictureDTO> createReceipePicture(@Valid @RequestBody ReceipePictureDTO receipePictureDTO)
        throws URISyntaxException {
        log.debug("REST request to save ReceipePicture : {}", receipePictureDTO);
        if (receipePictureDTO.getId() != null) {
            throw new BadRequestAlertException("A new receipePicture cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReceipePictureDTO result = receipePictureService.save(receipePictureDTO);
        return ResponseEntity
            .created(new URI("/api/receipe-pictures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /receipe-pictures/:id} : Updates an existing receipePicture.
     *
     * @param id the id of the receipePictureDTO to save.
     * @param receipePictureDTO the receipePictureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated receipePictureDTO,
     * or with status {@code 400 (Bad Request)} if the receipePictureDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the receipePictureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/receipe-pictures/{id}")
    public ResponseEntity<ReceipePictureDTO> updateReceipePicture(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ReceipePictureDTO receipePictureDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ReceipePicture : {}, {}", id, receipePictureDTO);
        if (receipePictureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, receipePictureDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!receipePictureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ReceipePictureDTO result = receipePictureService.save(receipePictureDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, receipePictureDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /receipe-pictures/:id} : Partial updates given fields of an existing receipePicture, field will ignore if it is null
     *
     * @param id the id of the receipePictureDTO to save.
     * @param receipePictureDTO the receipePictureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated receipePictureDTO,
     * or with status {@code 400 (Bad Request)} if the receipePictureDTO is not valid,
     * or with status {@code 404 (Not Found)} if the receipePictureDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the receipePictureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/receipe-pictures/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ReceipePictureDTO> partialUpdateReceipePicture(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ReceipePictureDTO receipePictureDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ReceipePicture partially : {}, {}", id, receipePictureDTO);
        if (receipePictureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, receipePictureDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!receipePictureRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ReceipePictureDTO> result = receipePictureService.partialUpdate(receipePictureDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, receipePictureDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /receipe-pictures} : get all the receipePictures.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of receipePictures in body.
     */
    @GetMapping("/receipe-pictures")
    public ResponseEntity<List<ReceipePictureDTO>> getAllReceipePictures(ReceipePictureCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ReceipePictures by criteria: {}", criteria);
        Page<ReceipePictureDTO> page = receipePictureQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /receipe-pictures/count} : count all the receipePictures.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/receipe-pictures/count")
    public ResponseEntity<Long> countReceipePictures(ReceipePictureCriteria criteria) {
        log.debug("REST request to count ReceipePictures by criteria: {}", criteria);
        return ResponseEntity.ok().body(receipePictureQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /receipe-pictures/:id} : get the "id" receipePicture.
     *
     * @param id the id of the receipePictureDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the receipePictureDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/receipe-pictures/{id}")
    public ResponseEntity<ReceipePictureDTO> getReceipePicture(@PathVariable Long id) {
        log.debug("REST request to get ReceipePicture : {}", id);
        Optional<ReceipePictureDTO> receipePictureDTO = receipePictureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(receipePictureDTO);
    }

    /**
     * {@code DELETE  /receipe-pictures/:id} : delete the "id" receipePicture.
     *
     * @param id the id of the receipePictureDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/receipe-pictures/{id}")
    public ResponseEntity<Void> deleteReceipePicture(@PathVariable Long id) {
        log.debug("REST request to delete ReceipePicture : {}", id);
        receipePictureService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
