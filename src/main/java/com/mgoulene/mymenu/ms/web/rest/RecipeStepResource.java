package com.mgoulene.mymenu.ms.web.rest;

import com.mgoulene.mymenu.ms.repository.RecipeStepRepository;
import com.mgoulene.mymenu.ms.service.RecipeStepQueryService;
import com.mgoulene.mymenu.ms.service.RecipeStepService;
import com.mgoulene.mymenu.ms.service.criteria.RecipeStepCriteria;
import com.mgoulene.mymenu.ms.service.dto.RecipeStepDTO;
import com.mgoulene.mymenu.ms.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.mymenu.ms.domain.RecipeStep}.
 */
@RestController
@RequestMapping("/api")
public class RecipeStepResource {

    private final Logger log = LoggerFactory.getLogger(RecipeStepResource.class);

    private static final String ENTITY_NAME = "mymenumsRecipeStep";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RecipeStepService recipeStepService;

    private final RecipeStepRepository recipeStepRepository;

    private final RecipeStepQueryService recipeStepQueryService;

    public RecipeStepResource(
        RecipeStepService recipeStepService,
        RecipeStepRepository recipeStepRepository,
        RecipeStepQueryService recipeStepQueryService
    ) {
        this.recipeStepService = recipeStepService;
        this.recipeStepRepository = recipeStepRepository;
        this.recipeStepQueryService = recipeStepQueryService;
    }

    /**
     * {@code POST  /recipe-steps} : Create a new recipeStep.
     *
     * @param recipeStepDTO the recipeStepDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new recipeStepDTO, or with status {@code 400 (Bad Request)} if the recipeStep has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/recipe-steps")
    public ResponseEntity<RecipeStepDTO> createRecipeStep(@Valid @RequestBody RecipeStepDTO recipeStepDTO) throws URISyntaxException {
        log.debug("REST request to save RecipeStep : {}", recipeStepDTO);
        if (recipeStepDTO.getId() != null) {
            throw new BadRequestAlertException("A new recipeStep cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecipeStepDTO result = recipeStepService.save(recipeStepDTO);
        return ResponseEntity
            .created(new URI("/api/recipe-steps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /recipe-steps/:id} : Updates an existing recipeStep.
     *
     * @param id the id of the recipeStepDTO to save.
     * @param recipeStepDTO the recipeStepDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recipeStepDTO,
     * or with status {@code 400 (Bad Request)} if the recipeStepDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the recipeStepDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/recipe-steps/{id}")
    public ResponseEntity<RecipeStepDTO> updateRecipeStep(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RecipeStepDTO recipeStepDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RecipeStep : {}, {}", id, recipeStepDTO);
        if (recipeStepDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recipeStepDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recipeStepRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RecipeStepDTO result = recipeStepService.save(recipeStepDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recipeStepDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /recipe-steps/:id} : Partial updates given fields of an existing recipeStep, field will ignore if it is null
     *
     * @param id the id of the recipeStepDTO to save.
     * @param recipeStepDTO the recipeStepDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated recipeStepDTO,
     * or with status {@code 400 (Bad Request)} if the recipeStepDTO is not valid,
     * or with status {@code 404 (Not Found)} if the recipeStepDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the recipeStepDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/recipe-steps/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<RecipeStepDTO> partialUpdateRecipeStep(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RecipeStepDTO recipeStepDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RecipeStep partially : {}, {}", id, recipeStepDTO);
        if (recipeStepDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, recipeStepDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!recipeStepRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RecipeStepDTO> result = recipeStepService.partialUpdate(recipeStepDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, recipeStepDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /recipe-steps} : get all the recipeSteps.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of recipeSteps in body.
     */
    @GetMapping("/recipe-steps")
    public ResponseEntity<List<RecipeStepDTO>> getAllRecipeSteps(RecipeStepCriteria criteria, Pageable pageable) {
        log.debug("REST request to get RecipeSteps by criteria: {}", criteria);
        Page<RecipeStepDTO> page = recipeStepQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /recipe-steps/count} : count all the recipeSteps.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/recipe-steps/count")
    public ResponseEntity<Long> countRecipeSteps(RecipeStepCriteria criteria) {
        log.debug("REST request to count RecipeSteps by criteria: {}", criteria);
        return ResponseEntity.ok().body(recipeStepQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /recipe-steps/:id} : get the "id" recipeStep.
     *
     * @param id the id of the recipeStepDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the recipeStepDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/recipe-steps/{id}")
    public ResponseEntity<RecipeStepDTO> getRecipeStep(@PathVariable Long id) {
        log.debug("REST request to get RecipeStep : {}", id);
        Optional<RecipeStepDTO> recipeStepDTO = recipeStepService.findOne(id);
        return ResponseUtil.wrapOrNotFound(recipeStepDTO);
    }

    /**
     * {@code DELETE  /recipe-steps/:id} : delete the "id" recipeStep.
     *
     * @param id the id of the recipeStepDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/recipe-steps/{id}")
    public ResponseEntity<Void> deleteRecipeStep(@PathVariable Long id) {
        log.debug("REST request to delete RecipeStep : {}", id);
        recipeStepService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
