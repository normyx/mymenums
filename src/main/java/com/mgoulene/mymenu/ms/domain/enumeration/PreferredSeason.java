package com.mgoulene.mymenu.ms.domain.enumeration;

/**
 * The PreferredSeason enumeration.
 */
public enum PreferredSeason {
    TRUE,
    FALSE,
}
