package com.mgoulene.mymenu.ms.domain.enumeration;

/**
 * The QuantityType enumeration.
 */
public enum QuantityType {
    QUANTITY,
    G,
    KG,
    L,
    ML,
    SOUPSPOON,
    TEASPON,
    PINCH,
}
