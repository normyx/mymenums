package com.mgoulene.mymenu.ms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Menu.
 */
@Entity
@Table(name = "menu")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tags", "ingredients", "recipePictures", "recipeSteps" }, allowSetters = true)
    private Recipe starter;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tags", "ingredients", "recipePictures", "recipeSteps" }, allowSetters = true)
    private Recipe main;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tags", "ingredients", "recipePictures", "recipeSteps" }, allowSetters = true)
    private Recipe dessert;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Menu id(Long id) {
        this.id = id;
        return this;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Menu date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getUserId() {
        return this.userId;
    }

    public Menu userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Recipe getStarter() {
        return this.starter;
    }

    public Menu starter(Recipe recipe) {
        this.setStarter(recipe);
        return this;
    }

    public void setStarter(Recipe recipe) {
        this.starter = recipe;
    }

    public Recipe getMain() {
        return this.main;
    }

    public Menu main(Recipe recipe) {
        this.setMain(recipe);
        return this;
    }

    public void setMain(Recipe recipe) {
        this.main = recipe;
    }

    public Recipe getDessert() {
        return this.dessert;
    }

    public Menu dessert(Recipe recipe) {
        this.setDessert(recipe);
        return this;
    }

    public void setDessert(Recipe recipe) {
        this.dessert = recipe;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Menu)) {
            return false;
        }
        return id != null && id.equals(((Menu) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Menu{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", userId='" + getUserId() + "'" +
            "}";
    }
}
