package com.mgoulene.mymenu.ms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ReceipePicture.
 */
@Entity
@Table(name = "receipe_picture")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ReceipePicture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Column(name = "picture", nullable = false)
    private byte[] picture;

    @Column(name = "picture_content_type", nullable = false)
    private String pictureContentType;

    @Size(max = 400)
    @Column(name = "label", length = 400)
    private String label;

    @NotNull
    @Min(value = 1)
    @Column(name = "jhi_order", nullable = false)
    private Integer order;

    @ManyToMany(mappedBy = "recipePictures")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tags", "ingredients", "recipePictures", "recipeSteps" }, allowSetters = true)
    private Set<Recipe> recipes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReceipePicture id(Long id) {
        this.id = id;
        return this;
    }

    public byte[] getPicture() {
        return this.picture;
    }

    public ReceipePicture picture(byte[] picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return this.pictureContentType;
    }

    public ReceipePicture pictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
        return this;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public String getLabel() {
        return this.label;
    }

    public ReceipePicture label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getOrder() {
        return this.order;
    }

    public ReceipePicture order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Set<Recipe> getRecipes() {
        return this.recipes;
    }

    public ReceipePicture recipes(Set<Recipe> recipes) {
        this.setRecipes(recipes);
        return this;
    }

    public ReceipePicture addRecipe(Recipe recipe) {
        this.recipes.add(recipe);
        recipe.getRecipePictures().add(this);
        return this;
    }

    public ReceipePicture removeRecipe(Recipe recipe) {
        this.recipes.remove(recipe);
        recipe.getRecipePictures().remove(this);
        return this;
    }

    public void setRecipes(Set<Recipe> recipes) {
        if (this.recipes != null) {
            this.recipes.forEach(i -> i.removeRecipePicture(this));
        }
        if (recipes != null) {
            recipes.forEach(i -> i.addRecipePicture(this));
        }
        this.recipes = recipes;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReceipePicture)) {
            return false;
        }
        return id != null && id.equals(((ReceipePicture) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ReceipePicture{" +
            "id=" + getId() +
            ", picture='" + getPicture() + "'" +
            ", pictureContentType='" + getPictureContentType() + "'" +
            ", label='" + getLabel() + "'" +
            ", order=" + getOrder() +
            "}";
    }
}
