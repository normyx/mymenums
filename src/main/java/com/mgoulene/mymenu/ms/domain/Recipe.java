package com.mgoulene.mymenu.ms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mgoulene.mymenu.ms.domain.enumeration.PreferredSeason;
import com.mgoulene.mymenu.ms.domain.enumeration.RecipeType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The Recipe entity.\n@author A true hipster
 */
@Entity
@Table(name = "recipe")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 400)
    @Column(name = "name", length = 400, nullable = false)
    private String name;

    @Size(max = 10000)
    @Column(name = "description", length = 10000)
    private String description;

    @Min(value = 0)
    @Column(name = "preparation_duration")
    private Integer preparationDuration;

    @Min(value = 0)
    @Column(name = "rest_duraction")
    private Integer restDuraction;

    @Min(value = 0)
    @Column(name = "cooking_duration")
    private Integer cookingDuration;

    @Enumerated(EnumType.STRING)
    @Column(name = "winter_preferrency")
    private PreferredSeason winterPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "spring_preferrency")
    private PreferredSeason springPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "summer_preferrency")
    private PreferredSeason summerPreferrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "autumn_preferrency")
    private PreferredSeason autumnPreferrency;

    @Min(value = 1)
    @Column(name = "number_of_people")
    private Integer numberOfPeople;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "recipe_type", nullable = false)
    private RecipeType recipeType;

    @Size(min = 5, max = 200)
    @Column(name = "recipe_url", length = 200)
    private String recipeUrl;

    @Size(max = 40000)
    @Column(name = "short_steps", length = 40000)
    private String shortSteps;

    @NotNull
    @Column(name = "is_short_desc_recipe", nullable = false)
    private Boolean isShortDescRecipe;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "rel_recipe__tag", joinColumns = @JoinColumn(name = "recipe_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "rel_recipe__ingredient",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "ingredient_id")
    )
    @JsonIgnoreProperties(value = { "product", "recipes" }, allowSetters = true)
    private Set<Ingredient> ingredients = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "rel_recipe__recipe_picture",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "recipe_picture_id")
    )
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<ReceipePicture> recipePictures = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "rel_recipe__recipe_step",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "recipe_step_id")
    )
    @JsonIgnoreProperties(value = { "recipes" }, allowSetters = true)
    private Set<RecipeStep> recipeSteps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Recipe id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Recipe name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Recipe description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPreparationDuration() {
        return this.preparationDuration;
    }

    public Recipe preparationDuration(Integer preparationDuration) {
        this.preparationDuration = preparationDuration;
        return this;
    }

    public void setPreparationDuration(Integer preparationDuration) {
        this.preparationDuration = preparationDuration;
    }

    public Integer getRestDuraction() {
        return this.restDuraction;
    }

    public Recipe restDuraction(Integer restDuraction) {
        this.restDuraction = restDuraction;
        return this;
    }

    public void setRestDuraction(Integer restDuraction) {
        this.restDuraction = restDuraction;
    }

    public Integer getCookingDuration() {
        return this.cookingDuration;
    }

    public Recipe cookingDuration(Integer cookingDuration) {
        this.cookingDuration = cookingDuration;
        return this;
    }

    public void setCookingDuration(Integer cookingDuration) {
        this.cookingDuration = cookingDuration;
    }

    public PreferredSeason getWinterPreferrency() {
        return this.winterPreferrency;
    }

    public Recipe winterPreferrency(PreferredSeason winterPreferrency) {
        this.winterPreferrency = winterPreferrency;
        return this;
    }

    public void setWinterPreferrency(PreferredSeason winterPreferrency) {
        this.winterPreferrency = winterPreferrency;
    }

    public PreferredSeason getSpringPreferrency() {
        return this.springPreferrency;
    }

    public Recipe springPreferrency(PreferredSeason springPreferrency) {
        this.springPreferrency = springPreferrency;
        return this;
    }

    public void setSpringPreferrency(PreferredSeason springPreferrency) {
        this.springPreferrency = springPreferrency;
    }

    public PreferredSeason getSummerPreferrency() {
        return this.summerPreferrency;
    }

    public Recipe summerPreferrency(PreferredSeason summerPreferrency) {
        this.summerPreferrency = summerPreferrency;
        return this;
    }

    public void setSummerPreferrency(PreferredSeason summerPreferrency) {
        this.summerPreferrency = summerPreferrency;
    }

    public PreferredSeason getAutumnPreferrency() {
        return this.autumnPreferrency;
    }

    public Recipe autumnPreferrency(PreferredSeason autumnPreferrency) {
        this.autumnPreferrency = autumnPreferrency;
        return this;
    }

    public void setAutumnPreferrency(PreferredSeason autumnPreferrency) {
        this.autumnPreferrency = autumnPreferrency;
    }

    public Integer getNumberOfPeople() {
        return this.numberOfPeople;
    }

    public Recipe numberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
        return this;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public RecipeType getRecipeType() {
        return this.recipeType;
    }

    public Recipe recipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
        return this;
    }

    public void setRecipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
    }

    public String getRecipeUrl() {
        return this.recipeUrl;
    }

    public Recipe recipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
        return this;
    }

    public void setRecipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public String getShortSteps() {
        return this.shortSteps;
    }

    public Recipe shortSteps(String shortSteps) {
        this.shortSteps = shortSteps;
        return this;
    }

    public void setShortSteps(String shortSteps) {
        this.shortSteps = shortSteps;
    }

    public Boolean getIsShortDescRecipe() {
        return this.isShortDescRecipe;
    }

    public Recipe isShortDescRecipe(Boolean isShortDescRecipe) {
        this.isShortDescRecipe = isShortDescRecipe;
        return this;
    }

    public void setIsShortDescRecipe(Boolean isShortDescRecipe) {
        this.isShortDescRecipe = isShortDescRecipe;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public Recipe tags(Set<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public Recipe addTag(Tag tag) {
        this.tags.add(tag);
        tag.getRecipes().add(this);
        return this;
    }

    public Recipe removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getRecipes().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public Recipe ingredients(Set<Ingredient> ingredients) {
        this.setIngredients(ingredients);
        return this;
    }

    public Recipe addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
        ingredient.getRecipes().add(this);
        return this;
    }

    public Recipe removeIngredient(Ingredient ingredient) {
        this.ingredients.remove(ingredient);
        ingredient.getRecipes().remove(this);
        return this;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Set<ReceipePicture> getRecipePictures() {
        return this.recipePictures;
    }

    public Recipe recipePictures(Set<ReceipePicture> receipePictures) {
        this.setRecipePictures(receipePictures);
        return this;
    }

    public Recipe addRecipePicture(ReceipePicture receipePicture) {
        this.recipePictures.add(receipePicture);
        receipePicture.getRecipes().add(this);
        return this;
    }

    public Recipe removeRecipePicture(ReceipePicture receipePicture) {
        this.recipePictures.remove(receipePicture);
        receipePicture.getRecipes().remove(this);
        return this;
    }

    public void setRecipePictures(Set<ReceipePicture> receipePictures) {
        this.recipePictures = receipePictures;
    }

    public Set<RecipeStep> getRecipeSteps() {
        return this.recipeSteps;
    }

    public Recipe recipeSteps(Set<RecipeStep> recipeSteps) {
        this.setRecipeSteps(recipeSteps);
        return this;
    }

    public Recipe addRecipeStep(RecipeStep recipeStep) {
        this.recipeSteps.add(recipeStep);
        recipeStep.getRecipes().add(this);
        return this;
    }

    public Recipe removeRecipeStep(RecipeStep recipeStep) {
        this.recipeSteps.remove(recipeStep);
        recipeStep.getRecipes().remove(this);
        return this;
    }

    public void setRecipeSteps(Set<RecipeStep> recipeSteps) {
        this.recipeSteps = recipeSteps;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Recipe)) {
            return false;
        }
        return id != null && id.equals(((Recipe) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Recipe{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", preparationDuration=" + getPreparationDuration() +
            ", restDuraction=" + getRestDuraction() +
            ", cookingDuration=" + getCookingDuration() +
            ", winterPreferrency='" + getWinterPreferrency() + "'" +
            ", springPreferrency='" + getSpringPreferrency() + "'" +
            ", summerPreferrency='" + getSummerPreferrency() + "'" +
            ", autumnPreferrency='" + getAutumnPreferrency() + "'" +
            ", numberOfPeople=" + getNumberOfPeople() +
            ", recipeType='" + getRecipeType() + "'" +
            ", recipeUrl='" + getRecipeUrl() + "'" +
            ", shortSteps='" + getShortSteps() + "'" +
            ", isShortDescRecipe='" + getIsShortDescRecipe() + "'" +
            "}";
    }
}
