package com.mgoulene.mymenu.ms.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A RecipeStep.
 */
@Entity
@Table(name = "recipe_step")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RecipeStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 1)
    @Column(name = "step_num", nullable = false)
    private Integer stepNum;

    @Size(max = 4000)
    @Column(name = "step_description", length = 4000)
    private String stepDescription;

    @ManyToMany(mappedBy = "recipeSteps")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tags", "ingredients", "recipePictures", "recipeSteps" }, allowSetters = true)
    private Set<Recipe> recipes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecipeStep id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getStepNum() {
        return this.stepNum;
    }

    public RecipeStep stepNum(Integer stepNum) {
        this.stepNum = stepNum;
        return this;
    }

    public void setStepNum(Integer stepNum) {
        this.stepNum = stepNum;
    }

    public String getStepDescription() {
        return this.stepDescription;
    }

    public RecipeStep stepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
        return this;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    public Set<Recipe> getRecipes() {
        return this.recipes;
    }

    public RecipeStep recipes(Set<Recipe> recipes) {
        this.setRecipes(recipes);
        return this;
    }

    public RecipeStep addRecipe(Recipe recipe) {
        this.recipes.add(recipe);
        recipe.getRecipeSteps().add(this);
        return this;
    }

    public RecipeStep removeRecipe(Recipe recipe) {
        this.recipes.remove(recipe);
        recipe.getRecipeSteps().remove(this);
        return this;
    }

    public void setRecipes(Set<Recipe> recipes) {
        if (this.recipes != null) {
            this.recipes.forEach(i -> i.removeRecipeStep(this));
        }
        if (recipes != null) {
            recipes.forEach(i -> i.addRecipeStep(this));
        }
        this.recipes = recipes;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RecipeStep)) {
            return false;
        }
        return id != null && id.equals(((RecipeStep) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RecipeStep{" +
            "id=" + getId() +
            ", stepNum=" + getStepNum() +
            ", stepDescription='" + getStepDescription() + "'" +
            "}";
    }
}
