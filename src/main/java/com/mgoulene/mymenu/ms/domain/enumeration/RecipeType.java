package com.mgoulene.mymenu.ms.domain.enumeration;

/**
 * The RecipeType enumeration.
 */
public enum RecipeType {
    STARTER,
    MAIN,
    DESSERT,
}
