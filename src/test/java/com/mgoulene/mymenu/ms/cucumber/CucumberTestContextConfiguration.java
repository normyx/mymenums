package com.mgoulene.mymenu.ms.cucumber;

import com.mgoulene.mymenu.ms.MymenumsApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = MymenumsApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
