package com.mgoulene.mymenu.ms.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.ms.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SheldTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sheld.class);
        Sheld sheld1 = new Sheld();
        sheld1.setId(1L);
        Sheld sheld2 = new Sheld();
        sheld2.setId(sheld1.getId());
        assertThat(sheld1).isEqualTo(sheld2);
        sheld2.setId(2L);
        assertThat(sheld1).isNotEqualTo(sheld2);
        sheld1.setId(null);
        assertThat(sheld1).isNotEqualTo(sheld2);
    }
}
