package com.mgoulene.mymenu.ms.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.ms.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ReceipePictureTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReceipePicture.class);
        ReceipePicture receipePicture1 = new ReceipePicture();
        receipePicture1.setId(1L);
        ReceipePicture receipePicture2 = new ReceipePicture();
        receipePicture2.setId(receipePicture1.getId());
        assertThat(receipePicture1).isEqualTo(receipePicture2);
        receipePicture2.setId(2L);
        assertThat(receipePicture1).isNotEqualTo(receipePicture2);
        receipePicture1.setId(null);
        assertThat(receipePicture1).isNotEqualTo(receipePicture2);
    }
}
