package com.mgoulene.mymenu.ms.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mymenu.ms.IntegrationTest;
import com.mgoulene.mymenu.ms.domain.Sheld;
import com.mgoulene.mymenu.ms.repository.SheldRepository;
import com.mgoulene.mymenu.ms.service.criteria.SheldCriteria;
import com.mgoulene.mymenu.ms.service.dto.SheldDTO;
import com.mgoulene.mymenu.ms.service.mapper.SheldMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SheldResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SheldResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/shelds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SheldRepository sheldRepository;

    @Autowired
    private SheldMapper sheldMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSheldMockMvc;

    private Sheld sheld;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sheld createEntity(EntityManager em) {
        Sheld sheld = new Sheld().name(DEFAULT_NAME);
        return sheld;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sheld createUpdatedEntity(EntityManager em) {
        Sheld sheld = new Sheld().name(UPDATED_NAME);
        return sheld;
    }

    @BeforeEach
    public void initTest() {
        sheld = createEntity(em);
    }

    @Test
    @Transactional
    void createSheld() throws Exception {
        int databaseSizeBeforeCreate = sheldRepository.findAll().size();
        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);
        restSheldMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeCreate + 1);
        Sheld testSheld = sheldList.get(sheldList.size() - 1);
        assertThat(testSheld.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createSheldWithExistingId() throws Exception {
        // Create the Sheld with an existing ID
        sheld.setId(1L);
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        int databaseSizeBeforeCreate = sheldRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSheldMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sheldRepository.findAll().size();
        // set the field null
        sheld.setName(null);

        // Create the Sheld, which fails.
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        restSheldMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllShelds() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList
        restSheldMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sheld.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getSheld() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get the sheld
        restSheldMockMvc
            .perform(get(ENTITY_API_URL_ID, sheld.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sheld.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getSheldsByIdFiltering() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        Long id = sheld.getId();

        defaultSheldShouldBeFound("id.equals=" + id);
        defaultSheldShouldNotBeFound("id.notEquals=" + id);

        defaultSheldShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSheldShouldNotBeFound("id.greaterThan=" + id);

        defaultSheldShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSheldShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSheldsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name equals to DEFAULT_NAME
        defaultSheldShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the sheldList where name equals to UPDATED_NAME
        defaultSheldShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSheldsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name not equals to DEFAULT_NAME
        defaultSheldShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the sheldList where name not equals to UPDATED_NAME
        defaultSheldShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSheldsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSheldShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the sheldList where name equals to UPDATED_NAME
        defaultSheldShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSheldsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name is not null
        defaultSheldShouldBeFound("name.specified=true");

        // Get all the sheldList where name is null
        defaultSheldShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllSheldsByNameContainsSomething() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name contains DEFAULT_NAME
        defaultSheldShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the sheldList where name contains UPDATED_NAME
        defaultSheldShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllSheldsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        // Get all the sheldList where name does not contain DEFAULT_NAME
        defaultSheldShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the sheldList where name does not contain UPDATED_NAME
        defaultSheldShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSheldShouldBeFound(String filter) throws Exception {
        restSheldMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sheld.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restSheldMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSheldShouldNotBeFound(String filter) throws Exception {
        restSheldMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSheldMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSheld() throws Exception {
        // Get the sheld
        restSheldMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSheld() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();

        // Update the sheld
        Sheld updatedSheld = sheldRepository.findById(sheld.getId()).get();
        // Disconnect from session so that the updates on updatedSheld are not directly saved in db
        em.detach(updatedSheld);
        updatedSheld.name(UPDATED_NAME);
        SheldDTO sheldDTO = sheldMapper.toDto(updatedSheld);

        restSheldMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sheldDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isOk());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
        Sheld testSheld = sheldList.get(sheldList.size() - 1);
        assertThat(testSheld.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sheldDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSheldWithPatch() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();

        // Update the sheld using partial update
        Sheld partialUpdatedSheld = new Sheld();
        partialUpdatedSheld.setId(sheld.getId());

        partialUpdatedSheld.name(UPDATED_NAME);

        restSheldMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSheld.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSheld))
            )
            .andExpect(status().isOk());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
        Sheld testSheld = sheldList.get(sheldList.size() - 1);
        assertThat(testSheld.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateSheldWithPatch() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();

        // Update the sheld using partial update
        Sheld partialUpdatedSheld = new Sheld();
        partialUpdatedSheld.setId(sheld.getId());

        partialUpdatedSheld.name(UPDATED_NAME);

        restSheldMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSheld.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSheld))
            )
            .andExpect(status().isOk());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
        Sheld testSheld = sheldList.get(sheldList.size() - 1);
        assertThat(testSheld.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, sheldDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSheld() throws Exception {
        int databaseSizeBeforeUpdate = sheldRepository.findAll().size();
        sheld.setId(count.incrementAndGet());

        // Create the Sheld
        SheldDTO sheldDTO = sheldMapper.toDto(sheld);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSheldMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sheldDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sheld in the database
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSheld() throws Exception {
        // Initialize the database
        sheldRepository.saveAndFlush(sheld);

        int databaseSizeBeforeDelete = sheldRepository.findAll().size();

        // Delete the sheld
        restSheldMockMvc
            .perform(delete(ENTITY_API_URL_ID, sheld.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sheld> sheldList = sheldRepository.findAll();
        assertThat(sheldList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
