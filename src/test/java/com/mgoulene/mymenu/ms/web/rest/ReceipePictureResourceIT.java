package com.mgoulene.mymenu.ms.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mymenu.ms.IntegrationTest;
import com.mgoulene.mymenu.ms.domain.ReceipePicture;
import com.mgoulene.mymenu.ms.domain.Recipe;
import com.mgoulene.mymenu.ms.repository.ReceipePictureRepository;
import com.mgoulene.mymenu.ms.service.criteria.ReceipePictureCriteria;
import com.mgoulene.mymenu.ms.service.dto.ReceipePictureDTO;
import com.mgoulene.mymenu.ms.service.mapper.ReceipePictureMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ReceipePictureResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ReceipePictureResourceIT {

    private static final byte[] DEFAULT_PICTURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PICTURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PICTURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PICTURE_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;
    private static final Integer SMALLER_ORDER = 1 - 1;

    private static final String ENTITY_API_URL = "/api/receipe-pictures";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ReceipePictureRepository receipePictureRepository;

    @Autowired
    private ReceipePictureMapper receipePictureMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReceipePictureMockMvc;

    private ReceipePicture receipePicture;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReceipePicture createEntity(EntityManager em) {
        ReceipePicture receipePicture = new ReceipePicture()
            .picture(DEFAULT_PICTURE)
            .pictureContentType(DEFAULT_PICTURE_CONTENT_TYPE)
            .label(DEFAULT_LABEL)
            .order(DEFAULT_ORDER);
        return receipePicture;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReceipePicture createUpdatedEntity(EntityManager em) {
        ReceipePicture receipePicture = new ReceipePicture()
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);
        return receipePicture;
    }

    @BeforeEach
    public void initTest() {
        receipePicture = createEntity(em);
    }

    @Test
    @Transactional
    void createReceipePicture() throws Exception {
        int databaseSizeBeforeCreate = receipePictureRepository.findAll().size();
        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);
        restReceipePictureMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeCreate + 1);
        ReceipePicture testReceipePicture = receipePictureList.get(receipePictureList.size() - 1);
        assertThat(testReceipePicture.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testReceipePicture.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
        assertThat(testReceipePicture.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testReceipePicture.getOrder()).isEqualTo(DEFAULT_ORDER);
    }

    @Test
    @Transactional
    void createReceipePictureWithExistingId() throws Exception {
        // Create the ReceipePicture with an existing ID
        receipePicture.setId(1L);
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        int databaseSizeBeforeCreate = receipePictureRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restReceipePictureMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = receipePictureRepository.findAll().size();
        // set the field null
        receipePicture.setOrder(null);

        // Create the ReceipePicture, which fails.
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        restReceipePictureMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllReceipePictures() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(receipePicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));
    }

    @Test
    @Transactional
    void getReceipePicture() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get the receipePicture
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL_ID, receipePicture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(receipePicture.getId().intValue()))
            .andExpect(jsonPath("$.pictureContentType").value(DEFAULT_PICTURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.picture").value(Base64Utils.encodeToString(DEFAULT_PICTURE)))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER));
    }

    @Test
    @Transactional
    void getReceipePicturesByIdFiltering() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        Long id = receipePicture.getId();

        defaultReceipePictureShouldBeFound("id.equals=" + id);
        defaultReceipePictureShouldNotBeFound("id.notEquals=" + id);

        defaultReceipePictureShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultReceipePictureShouldNotBeFound("id.greaterThan=" + id);

        defaultReceipePictureShouldBeFound("id.lessThanOrEqual=" + id);
        defaultReceipePictureShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label equals to DEFAULT_LABEL
        defaultReceipePictureShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the receipePictureList where label equals to UPDATED_LABEL
        defaultReceipePictureShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label not equals to DEFAULT_LABEL
        defaultReceipePictureShouldNotBeFound("label.notEquals=" + DEFAULT_LABEL);

        // Get all the receipePictureList where label not equals to UPDATED_LABEL
        defaultReceipePictureShouldBeFound("label.notEquals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultReceipePictureShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the receipePictureList where label equals to UPDATED_LABEL
        defaultReceipePictureShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label is not null
        defaultReceipePictureShouldBeFound("label.specified=true");

        // Get all the receipePictureList where label is null
        defaultReceipePictureShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelContainsSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label contains DEFAULT_LABEL
        defaultReceipePictureShouldBeFound("label.contains=" + DEFAULT_LABEL);

        // Get all the receipePictureList where label contains UPDATED_LABEL
        defaultReceipePictureShouldNotBeFound("label.contains=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByLabelNotContainsSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where label does not contain DEFAULT_LABEL
        defaultReceipePictureShouldNotBeFound("label.doesNotContain=" + DEFAULT_LABEL);

        // Get all the receipePictureList where label does not contain UPDATED_LABEL
        defaultReceipePictureShouldBeFound("label.doesNotContain=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order equals to DEFAULT_ORDER
        defaultReceipePictureShouldBeFound("order.equals=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order equals to UPDATED_ORDER
        defaultReceipePictureShouldNotBeFound("order.equals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order not equals to DEFAULT_ORDER
        defaultReceipePictureShouldNotBeFound("order.notEquals=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order not equals to UPDATED_ORDER
        defaultReceipePictureShouldBeFound("order.notEquals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsInShouldWork() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order in DEFAULT_ORDER or UPDATED_ORDER
        defaultReceipePictureShouldBeFound("order.in=" + DEFAULT_ORDER + "," + UPDATED_ORDER);

        // Get all the receipePictureList where order equals to UPDATED_ORDER
        defaultReceipePictureShouldNotBeFound("order.in=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order is not null
        defaultReceipePictureShouldBeFound("order.specified=true");

        // Get all the receipePictureList where order is null
        defaultReceipePictureShouldNotBeFound("order.specified=false");
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order is greater than or equal to DEFAULT_ORDER
        defaultReceipePictureShouldBeFound("order.greaterThanOrEqual=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order is greater than or equal to UPDATED_ORDER
        defaultReceipePictureShouldNotBeFound("order.greaterThanOrEqual=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order is less than or equal to DEFAULT_ORDER
        defaultReceipePictureShouldBeFound("order.lessThanOrEqual=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order is less than or equal to SMALLER_ORDER
        defaultReceipePictureShouldNotBeFound("order.lessThanOrEqual=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order is less than DEFAULT_ORDER
        defaultReceipePictureShouldNotBeFound("order.lessThan=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order is less than UPDATED_ORDER
        defaultReceipePictureShouldBeFound("order.lessThan=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        // Get all the receipePictureList where order is greater than DEFAULT_ORDER
        defaultReceipePictureShouldNotBeFound("order.greaterThan=" + DEFAULT_ORDER);

        // Get all the receipePictureList where order is greater than SMALLER_ORDER
        defaultReceipePictureShouldBeFound("order.greaterThan=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    void getAllReceipePicturesByRecipeIsEqualToSomething() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);
        Recipe recipe = RecipeResourceIT.createEntity(em);
        em.persist(recipe);
        em.flush();
        receipePicture.addRecipe(recipe);
        receipePictureRepository.saveAndFlush(receipePicture);
        Long recipeId = recipe.getId();

        // Get all the receipePictureList where recipe equals to recipeId
        defaultReceipePictureShouldBeFound("recipeId.equals=" + recipeId);

        // Get all the receipePictureList where recipe equals to (recipeId + 1)
        defaultReceipePictureShouldNotBeFound("recipeId.equals=" + (recipeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultReceipePictureShouldBeFound(String filter) throws Exception {
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(receipePicture.getId().intValue())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)));

        // Check, that the count call also returns 1
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultReceipePictureShouldNotBeFound(String filter) throws Exception {
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restReceipePictureMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingReceipePicture() throws Exception {
        // Get the receipePicture
        restReceipePictureMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewReceipePicture() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();

        // Update the receipePicture
        ReceipePicture updatedReceipePicture = receipePictureRepository.findById(receipePicture.getId()).get();
        // Disconnect from session so that the updates on updatedReceipePicture are not directly saved in db
        em.detach(updatedReceipePicture);
        updatedReceipePicture
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(updatedReceipePicture);

        restReceipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, receipePictureDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isOk());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
        ReceipePicture testReceipePicture = receipePictureList.get(receipePictureList.size() - 1);
        assertThat(testReceipePicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testReceipePicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testReceipePicture.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testReceipePicture.getOrder()).isEqualTo(UPDATED_ORDER);
    }

    @Test
    @Transactional
    void putNonExistingReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, receipePictureDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateReceipePictureWithPatch() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();

        // Update the receipePicture using partial update
        ReceipePicture partialUpdatedReceipePicture = new ReceipePicture();
        partialUpdatedReceipePicture.setId(receipePicture.getId());

        partialUpdatedReceipePicture.order(UPDATED_ORDER);

        restReceipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedReceipePicture.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedReceipePicture))
            )
            .andExpect(status().isOk());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
        ReceipePicture testReceipePicture = receipePictureList.get(receipePictureList.size() - 1);
        assertThat(testReceipePicture.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testReceipePicture.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
        assertThat(testReceipePicture.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testReceipePicture.getOrder()).isEqualTo(UPDATED_ORDER);
    }

    @Test
    @Transactional
    void fullUpdateReceipePictureWithPatch() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();

        // Update the receipePicture using partial update
        ReceipePicture partialUpdatedReceipePicture = new ReceipePicture();
        partialUpdatedReceipePicture.setId(receipePicture.getId());

        partialUpdatedReceipePicture
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .label(UPDATED_LABEL)
            .order(UPDATED_ORDER);

        restReceipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedReceipePicture.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedReceipePicture))
            )
            .andExpect(status().isOk());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
        ReceipePicture testReceipePicture = receipePictureList.get(receipePictureList.size() - 1);
        assertThat(testReceipePicture.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testReceipePicture.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testReceipePicture.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testReceipePicture.getOrder()).isEqualTo(UPDATED_ORDER);
    }

    @Test
    @Transactional
    void patchNonExistingReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, receipePictureDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamReceipePicture() throws Exception {
        int databaseSizeBeforeUpdate = receipePictureRepository.findAll().size();
        receipePicture.setId(count.incrementAndGet());

        // Create the ReceipePicture
        ReceipePictureDTO receipePictureDTO = receipePictureMapper.toDto(receipePicture);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restReceipePictureMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(receipePictureDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ReceipePicture in the database
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteReceipePicture() throws Exception {
        // Initialize the database
        receipePictureRepository.saveAndFlush(receipePicture);

        int databaseSizeBeforeDelete = receipePictureRepository.findAll().size();

        // Delete the receipePicture
        restReceipePictureMockMvc
            .perform(delete(ENTITY_API_URL_ID, receipePicture.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ReceipePicture> receipePictureList = receipePictureRepository.findAll();
        assertThat(receipePictureList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
