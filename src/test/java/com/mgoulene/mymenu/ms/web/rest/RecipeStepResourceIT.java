package com.mgoulene.mymenu.ms.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mymenu.ms.IntegrationTest;
import com.mgoulene.mymenu.ms.domain.Recipe;
import com.mgoulene.mymenu.ms.domain.RecipeStep;
import com.mgoulene.mymenu.ms.repository.RecipeStepRepository;
import com.mgoulene.mymenu.ms.service.criteria.RecipeStepCriteria;
import com.mgoulene.mymenu.ms.service.dto.RecipeStepDTO;
import com.mgoulene.mymenu.ms.service.mapper.RecipeStepMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RecipeStepResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RecipeStepResourceIT {

    private static final Integer DEFAULT_STEP_NUM = 1;
    private static final Integer UPDATED_STEP_NUM = 2;
    private static final Integer SMALLER_STEP_NUM = 1 - 1;

    private static final String DEFAULT_STEP_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_STEP_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/recipe-steps";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RecipeStepRepository recipeStepRepository;

    @Autowired
    private RecipeStepMapper recipeStepMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRecipeStepMockMvc;

    private RecipeStep recipeStep;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipeStep createEntity(EntityManager em) {
        RecipeStep recipeStep = new RecipeStep().stepNum(DEFAULT_STEP_NUM).stepDescription(DEFAULT_STEP_DESCRIPTION);
        return recipeStep;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RecipeStep createUpdatedEntity(EntityManager em) {
        RecipeStep recipeStep = new RecipeStep().stepNum(UPDATED_STEP_NUM).stepDescription(UPDATED_STEP_DESCRIPTION);
        return recipeStep;
    }

    @BeforeEach
    public void initTest() {
        recipeStep = createEntity(em);
    }

    @Test
    @Transactional
    void createRecipeStep() throws Exception {
        int databaseSizeBeforeCreate = recipeStepRepository.findAll().size();
        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);
        restRecipeStepMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeCreate + 1);
        RecipeStep testRecipeStep = recipeStepList.get(recipeStepList.size() - 1);
        assertThat(testRecipeStep.getStepNum()).isEqualTo(DEFAULT_STEP_NUM);
        assertThat(testRecipeStep.getStepDescription()).isEqualTo(DEFAULT_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void createRecipeStepWithExistingId() throws Exception {
        // Create the RecipeStep with an existing ID
        recipeStep.setId(1L);
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        int databaseSizeBeforeCreate = recipeStepRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecipeStepMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkStepNumIsRequired() throws Exception {
        int databaseSizeBeforeTest = recipeStepRepository.findAll().size();
        // set the field null
        recipeStep.setStepNum(null);

        // Create the RecipeStep, which fails.
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        restRecipeStepMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRecipeSteps() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipeStep.getId().intValue())))
            .andExpect(jsonPath("$.[*].stepNum").value(hasItem(DEFAULT_STEP_NUM)))
            .andExpect(jsonPath("$.[*].stepDescription").value(hasItem(DEFAULT_STEP_DESCRIPTION)));
    }

    @Test
    @Transactional
    void getRecipeStep() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get the recipeStep
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL_ID, recipeStep.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(recipeStep.getId().intValue()))
            .andExpect(jsonPath("$.stepNum").value(DEFAULT_STEP_NUM))
            .andExpect(jsonPath("$.stepDescription").value(DEFAULT_STEP_DESCRIPTION));
    }

    @Test
    @Transactional
    void getRecipeStepsByIdFiltering() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        Long id = recipeStep.getId();

        defaultRecipeStepShouldBeFound("id.equals=" + id);
        defaultRecipeStepShouldNotBeFound("id.notEquals=" + id);

        defaultRecipeStepShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultRecipeStepShouldNotBeFound("id.greaterThan=" + id);

        defaultRecipeStepShouldBeFound("id.lessThanOrEqual=" + id);
        defaultRecipeStepShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum equals to DEFAULT_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.equals=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum equals to UPDATED_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.equals=" + UPDATED_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum not equals to DEFAULT_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.notEquals=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum not equals to UPDATED_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.notEquals=" + UPDATED_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsInShouldWork() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum in DEFAULT_STEP_NUM or UPDATED_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.in=" + DEFAULT_STEP_NUM + "," + UPDATED_STEP_NUM);

        // Get all the recipeStepList where stepNum equals to UPDATED_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.in=" + UPDATED_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum is not null
        defaultRecipeStepShouldBeFound("stepNum.specified=true");

        // Get all the recipeStepList where stepNum is null
        defaultRecipeStepShouldNotBeFound("stepNum.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum is greater than or equal to DEFAULT_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.greaterThanOrEqual=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum is greater than or equal to UPDATED_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.greaterThanOrEqual=" + UPDATED_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum is less than or equal to DEFAULT_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.lessThanOrEqual=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum is less than or equal to SMALLER_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.lessThanOrEqual=" + SMALLER_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsLessThanSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum is less than DEFAULT_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.lessThan=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum is less than UPDATED_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.lessThan=" + UPDATED_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepNumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepNum is greater than DEFAULT_STEP_NUM
        defaultRecipeStepShouldNotBeFound("stepNum.greaterThan=" + DEFAULT_STEP_NUM);

        // Get all the recipeStepList where stepNum is greater than SMALLER_STEP_NUM
        defaultRecipeStepShouldBeFound("stepNum.greaterThan=" + SMALLER_STEP_NUM);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription equals to DEFAULT_STEP_DESCRIPTION
        defaultRecipeStepShouldBeFound("stepDescription.equals=" + DEFAULT_STEP_DESCRIPTION);

        // Get all the recipeStepList where stepDescription equals to UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldNotBeFound("stepDescription.equals=" + UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription not equals to DEFAULT_STEP_DESCRIPTION
        defaultRecipeStepShouldNotBeFound("stepDescription.notEquals=" + DEFAULT_STEP_DESCRIPTION);

        // Get all the recipeStepList where stepDescription not equals to UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldBeFound("stepDescription.notEquals=" + UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription in DEFAULT_STEP_DESCRIPTION or UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldBeFound("stepDescription.in=" + DEFAULT_STEP_DESCRIPTION + "," + UPDATED_STEP_DESCRIPTION);

        // Get all the recipeStepList where stepDescription equals to UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldNotBeFound("stepDescription.in=" + UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription is not null
        defaultRecipeStepShouldBeFound("stepDescription.specified=true");

        // Get all the recipeStepList where stepDescription is null
        defaultRecipeStepShouldNotBeFound("stepDescription.specified=false");
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionContainsSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription contains DEFAULT_STEP_DESCRIPTION
        defaultRecipeStepShouldBeFound("stepDescription.contains=" + DEFAULT_STEP_DESCRIPTION);

        // Get all the recipeStepList where stepDescription contains UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldNotBeFound("stepDescription.contains=" + UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByStepDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        // Get all the recipeStepList where stepDescription does not contain DEFAULT_STEP_DESCRIPTION
        defaultRecipeStepShouldNotBeFound("stepDescription.doesNotContain=" + DEFAULT_STEP_DESCRIPTION);

        // Get all the recipeStepList where stepDescription does not contain UPDATED_STEP_DESCRIPTION
        defaultRecipeStepShouldBeFound("stepDescription.doesNotContain=" + UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllRecipeStepsByRecipeIsEqualToSomething() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);
        Recipe recipe = RecipeResourceIT.createEntity(em);
        em.persist(recipe);
        em.flush();
        recipeStep.addRecipe(recipe);
        recipeStepRepository.saveAndFlush(recipeStep);
        Long recipeId = recipe.getId();

        // Get all the recipeStepList where recipe equals to recipeId
        defaultRecipeStepShouldBeFound("recipeId.equals=" + recipeId);

        // Get all the recipeStepList where recipe equals to (recipeId + 1)
        defaultRecipeStepShouldNotBeFound("recipeId.equals=" + (recipeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRecipeStepShouldBeFound(String filter) throws Exception {
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(recipeStep.getId().intValue())))
            .andExpect(jsonPath("$.[*].stepNum").value(hasItem(DEFAULT_STEP_NUM)))
            .andExpect(jsonPath("$.[*].stepDescription").value(hasItem(DEFAULT_STEP_DESCRIPTION)));

        // Check, that the count call also returns 1
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRecipeStepShouldNotBeFound(String filter) throws Exception {
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRecipeStepMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingRecipeStep() throws Exception {
        // Get the recipeStep
        restRecipeStepMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRecipeStep() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();

        // Update the recipeStep
        RecipeStep updatedRecipeStep = recipeStepRepository.findById(recipeStep.getId()).get();
        // Disconnect from session so that the updates on updatedRecipeStep are not directly saved in db
        em.detach(updatedRecipeStep);
        updatedRecipeStep.stepNum(UPDATED_STEP_NUM).stepDescription(UPDATED_STEP_DESCRIPTION);
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(updatedRecipeStep);

        restRecipeStepMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipeStepDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isOk());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
        RecipeStep testRecipeStep = recipeStepList.get(recipeStepList.size() - 1);
        assertThat(testRecipeStep.getStepNum()).isEqualTo(UPDATED_STEP_NUM);
        assertThat(testRecipeStep.getStepDescription()).isEqualTo(UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                put(ENTITY_API_URL_ID, recipeStepDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRecipeStepWithPatch() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();

        // Update the recipeStep using partial update
        RecipeStep partialUpdatedRecipeStep = new RecipeStep();
        partialUpdatedRecipeStep.setId(recipeStep.getId());

        partialUpdatedRecipeStep.stepNum(UPDATED_STEP_NUM);

        restRecipeStepMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipeStep.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipeStep))
            )
            .andExpect(status().isOk());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
        RecipeStep testRecipeStep = recipeStepList.get(recipeStepList.size() - 1);
        assertThat(testRecipeStep.getStepNum()).isEqualTo(UPDATED_STEP_NUM);
        assertThat(testRecipeStep.getStepDescription()).isEqualTo(DEFAULT_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateRecipeStepWithPatch() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();

        // Update the recipeStep using partial update
        RecipeStep partialUpdatedRecipeStep = new RecipeStep();
        partialUpdatedRecipeStep.setId(recipeStep.getId());

        partialUpdatedRecipeStep.stepNum(UPDATED_STEP_NUM).stepDescription(UPDATED_STEP_DESCRIPTION);

        restRecipeStepMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRecipeStep.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRecipeStep))
            )
            .andExpect(status().isOk());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
        RecipeStep testRecipeStep = recipeStepList.get(recipeStepList.size() - 1);
        assertThat(testRecipeStep.getStepNum()).isEqualTo(UPDATED_STEP_NUM);
        assertThat(testRecipeStep.getStepDescription()).isEqualTo(UPDATED_STEP_DESCRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, recipeStepDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRecipeStep() throws Exception {
        int databaseSizeBeforeUpdate = recipeStepRepository.findAll().size();
        recipeStep.setId(count.incrementAndGet());

        // Create the RecipeStep
        RecipeStepDTO recipeStepDTO = recipeStepMapper.toDto(recipeStep);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRecipeStepMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(recipeStepDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RecipeStep in the database
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRecipeStep() throws Exception {
        // Initialize the database
        recipeStepRepository.saveAndFlush(recipeStep);

        int databaseSizeBeforeDelete = recipeStepRepository.findAll().size();

        // Delete the recipeStep
        restRecipeStepMockMvc
            .perform(delete(ENTITY_API_URL_ID, recipeStep.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RecipeStep> recipeStepList = recipeStepRepository.findAll();
        assertThat(recipeStepList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
