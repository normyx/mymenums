package com.mgoulene.mymenu.ms.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.ms.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RecipeStepDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RecipeStepDTO.class);
        RecipeStepDTO recipeStepDTO1 = new RecipeStepDTO();
        recipeStepDTO1.setId(1L);
        RecipeStepDTO recipeStepDTO2 = new RecipeStepDTO();
        assertThat(recipeStepDTO1).isNotEqualTo(recipeStepDTO2);
        recipeStepDTO2.setId(recipeStepDTO1.getId());
        assertThat(recipeStepDTO1).isEqualTo(recipeStepDTO2);
        recipeStepDTO2.setId(2L);
        assertThat(recipeStepDTO1).isNotEqualTo(recipeStepDTO2);
        recipeStepDTO1.setId(null);
        assertThat(recipeStepDTO1).isNotEqualTo(recipeStepDTO2);
    }
}
