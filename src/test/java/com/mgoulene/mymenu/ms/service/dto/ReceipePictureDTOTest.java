package com.mgoulene.mymenu.ms.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mymenu.ms.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ReceipePictureDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReceipePictureDTO.class);
        ReceipePictureDTO receipePictureDTO1 = new ReceipePictureDTO();
        receipePictureDTO1.setId(1L);
        ReceipePictureDTO receipePictureDTO2 = new ReceipePictureDTO();
        assertThat(receipePictureDTO1).isNotEqualTo(receipePictureDTO2);
        receipePictureDTO2.setId(receipePictureDTO1.getId());
        assertThat(receipePictureDTO1).isEqualTo(receipePictureDTO2);
        receipePictureDTO2.setId(2L);
        assertThat(receipePictureDTO1).isNotEqualTo(receipePictureDTO2);
        receipePictureDTO1.setId(null);
        assertThat(receipePictureDTO1).isNotEqualTo(receipePictureDTO2);
    }
}
