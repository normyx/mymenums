package com.mgoulene.mymenu.ms;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.mgoulene.mymenu.ms");

        noClasses()
            .that()
            .resideInAnyPackage("com.mgoulene.mymenu.ms.service..")
            .or()
            .resideInAnyPackage("com.mgoulene.mymenu.ms.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.mgoulene.mymenu.ms.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
